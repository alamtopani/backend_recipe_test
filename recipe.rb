require 'json'
require "ostruct"

class Recipe
  def initialize
    @recipes = []
  end

  class << self
    # To Add New Recipe
    def recipe(_recipe)
      new_recipe = {name: _recipe, ingredients: [], method_steps: []}
      @recipes << new_recipe
      Recipe.update_data(@recipes)
      puts "success add new recipe '#{_recipe}'"
    end

    # To Add New Ingredients on Recipe
    def ingredient(_ingredient)
      recipe = @recipes[@recipes.count-1]
      recipe[:ingredients] << _ingredient
      Recipe.update_data(@recipes)
      puts "success add new ingredient '#{_ingredient}'"
    end

    # To Add New Steps on Recipe
    def step(_step)
      recipe = @recipes[@recipes.count-1]
      recipe[:method_steps] << _step
      Recipe.update_data(@recipes)
      puts "success add new step '#{_step}'"
    end

    # To Get Data Recipe by Name
    def for(_recipe)
      @recipes = JSON.parse(File.read('recipes.json'))
      @recipe = @recipes.select{|r| r['name'] == _recipe}[0]
      puts "get data #{_recipe}"
      @data = OpenStruct.new(@recipe)
    end

    # To Clear Data Recipe
    def clear
      @recipes = []
      Recipe.update_data(@recipes)
      puts 'clear data'
    end
    
    # To Update Data Recipe
    def update_data(data)
      File.open('recipes.json',"w") do |f|
        f.write(data.to_json)
      end
    end
  end
end

def recipe(_recipe)
  Recipe.recipe(_recipe)
end

def ingredient(_ingredient)
  Recipe.ingredient(_ingredient)
end

def step(_step)
  Recipe.step(_step)
end